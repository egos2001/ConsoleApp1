﻿using System;

namespace ConsoleApp1
{
    public class Program
    {
        static bool Operate(int a, int b, int sign)
        {
            switch (sign)
            {
                case 0:
                    Console.WriteLine($"Result of {a} + {b}  is  {a + b}");

                    return true;

                case 1:
                    Console.WriteLine($"Result of {a} - {b}  is  {a - b}");

                    return true;

                case 2:
                    Console.WriteLine($"Result of {a} * {b}  is  {a * b}");

                    return true;

                case 3:
                    if (b == 0)
                    {
                        Console.WriteLine("invalid value");
                        return false;
                    }
                    else
                    {
                        Console.WriteLine($"Result of {a} / {b}  is  {(float)a / b}");

                        return true;

                    }
                default:
                    Console.WriteLine("Invalid Operator");
                    return true;

            }
        }

        static void Main(string[] args)
        {

            Console.WriteLine("Enter the operands");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the operator: 0 = +, 1 = -, 2 = *, 3 = /");
            int sign = Convert.ToInt32(Console.ReadLine());
            Operate(a, b, sign);
            Console.ReadKey();
        }
    }

}